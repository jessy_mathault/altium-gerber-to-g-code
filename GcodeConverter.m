function [ Gcommands ] = GcodeConverter( data , optimize)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
    convFACTOR = 1000;

    file = data.data;
    sv = data.sharedData;


    Gcommands{1} = ['G92 X',num2str(sv.xi/convFACTOR),' Y', num2str(sv.yi/convFACTOR)];
    Gcommands{2} = ['G01 X',num2str(sv.xi/convFACTOR),' Y', num2str(sv.yi/convFACTOR),' F*SETSPEEDLIN*'];
    Gcommands{3} = ['G01 Z*DOWN* F*SETSPEEDLIN*'];
    
    line  = 1;

    while line<size(file,2)
        if ~strcmp(file{line}{1}.char,'G')
            commandType = 1;
            subInd = 0;
            
        else
            commandType = file{line}{1}.num;
            subInd = 1;
        end
        

        %% G01 **
        if commandType == 1
            %read lines until a new command type
            while (file{line+subInd}{1}.char ~= 'G')&&(file{line+subInd}{1}.char ~= 'M')
                x = sv.lastx;
                y = sv.lasty;
                f = '*SETSPEEDLIN*';
                disableDraw = 0;
                tempx = sv.lastx;
                tempy = sv.lasty;
                for index = 1:size(file{line+subInd},2)
                    char = file{line+subInd}{index}.char;
                    num = file{line+subInd}{index}.num; 
                    if char == 'X'
                        x = num;
                        sv.lastx = x;
                    elseif char == 'Y'
                        y = num;
                        sv.lasty = y;
                    elseif char == 'D'
                        if num == 1
                        else
                            disableDraw  = 1;
                        end
                    else
                        disp('something is wrong, G-codeGen G01')
                        disp(['found char : ',char,' and num : ', num2str(num)]);
                    end
                end
                %disp((tempx==sv.lastx)&&(tempy==sv.lasty)&&optimize)
                if ~((abs(tempx/convFACTOR-sv.lastx/convFACTOR)<0.05)&&(abs(tempy/convFACTOR-sv.lasty/convFACTOR)<0.2)&&optimize)
                    if disableDraw
                        Gcommands{end+1} = ['G01 Z*UP* F',f];
                    end
                    Gcommands{end+1} = ['G01 X',num2str(x/convFACTOR),' Y',num2str(y/convFACTOR), ' F', f];
                    if disableDraw
                        Gcommands{end+1} = ['G01 Z*DOWN* F',f];
                    end
                    subInd = subInd +1;
                else
                    subInd = subInd +1;
                end
            end


        %% G02
        elseif commandType == 2
            %read lines until a new command type
            while (file{line+subInd}{1}.char ~= 'G')&&(file{line+subInd}{1}.char ~= 'M')
                x = sv.lastx;
                y = sv.lasty;
                i = sv.lic;
                j = sv.ljc;
                f = '*SETSPEEDCIRC*';
                disableDraw = 0;

                for index = 1:size(file{line+subInd},2)

                    char = file{line+subInd}{index}.char;
                    num = file{line+subInd}{index}.num; 
                    if char == 'X'
                        x = num;
                        sv.lastx = x;
                    elseif char == 'Y'
                        y = num;
                        sv.lasty = y;
                    elseif char == 'I'
                        i = num;
                        sv.lic = i;
                    elseif char == 'J'
                        j = num;
                        sv.ljc = j;
                    elseif char == 'D'
                        if num == 1
                        else
                            disableDraw  = 1;
                        end
                    else
                        disp('something is wrong, G-codeGen G01')
                        disp(['found char : ',char,' and num : ', num2str(num)]);
                    end
                end

                if disableDraw
                    Gcommands{end+1} = ['G01 Z*UP* F',f];
                end
                Gcommands{end+1} = ['G02 X',num2str(x/convFACTOR),' Y',num2str(y/convFACTOR),' I',num2str(i/convFACTOR),' J',num2str(j/convFACTOR), ' F', f];
                if disableDraw
                    Gcommands{end+1} = ['G01 Z*DOWN* F',f];
                end
                subInd = subInd +1;
            end 


        %% G03
        elseif commandType == 3
            %read lines until a new command type
            while (file{line+subInd}{1}.char ~= 'G')&&(file{line+subInd}{1}.char ~= 'M')
                x = sv.lastx;
                y = sv.lasty;
                i = sv.lic;
                j = sv.ljc;
                f = '*SETSPEEDCIRC*';
                disableDraw = 0;

                for index = 1:size(file{line+subInd},2)
                    %disp(index)
                    char = file{line+subInd}{index}.char;
                    num = file{line+subInd}{index}.num; 
                    if char == 'X'
                        x = num;
                        sv.lastx = x;
                    elseif char == 'Y'
                        y = num;
                        sv.lasty = y;
                    elseif char == 'I'
                        i = num;
                        sv.lic = i;
                    elseif char == 'J'
                        j = num;
                        sv.ljc = j;
                    elseif char == 'D'
                        if num == 1
                        else
                            disableDraw  = 1;
                        end
                    else
                        disp('something is wrong, G-codeGen G01')
                        disp(['found char : ',char,' and num : ', num2str(num)]);
                    end
                end

                if disableDraw
                    Gcommands{end+1} = ['G01 Z*UP* F',f];
                end
                Gcommands{end+1} = ['G03 X',num2str(x/convFACTOR),' Y',num2str(y/convFACTOR),' I',num2str(i/convFACTOR),' J',num2str(j/convFACTOR), ' F', f];
                if disableDraw
                    Gcommands{end+1} = ['G01 Z*DOWN* F',f];
                end
                subInd = subInd +1;
            end
        end 

        line  = line + subInd ;

    end
    
    Gcommands{end+1} = ['G01 Z*FINAL* F*SETSPEEDLIN*'];
    Gcommands{end+1} = ['G01 X',num2str(sv.xi/convFACTOR),' Y', num2str(sv.yi/convFACTOR),'F*SETSPEEDLIN*'];

    for x = 1:size(Gcommands,2)
        disp(Gcommands{x})
    end

end

