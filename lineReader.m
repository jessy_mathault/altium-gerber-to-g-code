function [ data ] = lineReader( string )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes her
    data = {};
    freetoken = 0;
    indexes = regexp(string, '[A-Z].[0-9]*');
    stars = strfind(string,'*');
    if size(stars,2) == 1
        indexes(end+1) = strfind(string,'*');
    else
        indexes = [indexes(end), stars(end)];
        disp(['Double * in line, only the last one kept: ',string]);
        freetoken = 1;
    end
    nG7X = 0;
    for x = 1:size(indexes,2)-1
        char = string(indexes(x));
        num = str2num(string(indexes(x)+1:indexes(x+1)-1));
        data{x}.char = char;
        data{x}.num = num;
    end


    strRebuilt = '';

    for x = 1:size(data,2)
        if ~((strcmp(data{x}.char, 'G')||(strcmp(data{x}.char, 'D'))||(strcmp(data{x}.char, 'M'))))
            strRebuilt = horzcat(strRebuilt, data{x}.char, num2str(data{x}.num));
        else
            strRebuilt = horzcat(strRebuilt, data{x}.char, num2str(data{x}.num,'%02i'));
        end
    end
    strRebuilt(end+1) = '*';
    %disp(strRebuilt)
    if ~freetoken
        if ~strcmp(string, strRebuilt)
            ME = MException('myComponent:inputError', ...
            'Error reading this line : %s',string);
            throw(ME)
        end
    end
        

end

