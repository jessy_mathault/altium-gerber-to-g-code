function [ file ] = fileReader( fileName )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    fid = fopen(fileName);
    line = fgetl(fid);
    while ~(strcmp(line, 'D10*')||(strcmp(line, 'G54D10*')))
        assert(ischar(line))
        line = fgetl(fid);
    end

    firstLine = fgetl(fid);
    initData = lineReader(firstLine);

    sharedData.xoff = initData{1}.num;
    sharedData.yoff = initData{2}.num;
    sharedData.xi = initData{1}.num;
    sharedData.yi = initData{2}.num;
    sharedData.lastx = initData{1}.num;
    sharedData.lasty = initData{2}.num;
    sharedData.lic = 0;
    sharedData.ljc = 0;

    file.sharedData = sharedData;
    file.data = {};
    
    nextLine = fgetl(fid);
    while ischar(nextLine)
        lineData = lineReader(nextLine);
        expand = 0;
        for x = 1:size(lineData,2)
            %disp(lineData{x}.char)
            if lineData{x}.char == 'X'
                %lineData{x}.num = lineData{x}.num - sharedData.xoff;
                %disp('modifying')
            elseif lineData{x}.char == 'Y'
                %lineData{x}.num = lineData{x}.num - sharedData.yoff;
                %disp('modifying')
            end
            
            if ((lineData{x}.char =='G')&&((lineData{x}.num ==75)||(lineData{x}.num ==74)))
                lineData(x) = []; 
            end
            
        end
        if ~isempty(lineData)
            if (lineData{1}.char =='G')&&(size(lineData,2)>1)
                expand = 1;
            end
            if expand
                file.data{end+1} = {lineData{1}};
                file.data{end+1} = {lineData{2:end}};
            else
                file.data{end+1} = lineData;
            end
        end
        nextLine = fgetl(fid);
    end

    
    fclose(fid);

end

