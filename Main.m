clear
clc

fileToOpen = 'wingV3.gtl';
fileToWrite = 'wingV3.gCode';
speedLin = 300; %[mm/s] linear speed
speedCirc = 300; %[mm/s] speed of arcs
Zdist = 25; %set the z drop to -Zdist [mm] (G01 Z-Zdist FspeedLin)

GcodArchitecture = GcodeConverter(fileReader(fileToOpen),1); %Gerber filename, optimize option (1=true, 0 false))
%GcodArchitecture = GcodeConverter(fileReader(['.\ALTIUM\',file,'.GTL']),1);

%fileName = ['.\gcodes\',file,'.txt'];

fid = fopen(fileToWrite, 'w');

%fprintf(fid,'G92 \n');
%fprintf(fid,'G01 X0 Y0 Z0 F15 \n');
%fprintf(fid,'G01 X122.629 Y81.968 Z0 F15 \n');
for x = 1:size(GcodArchitecture,2)
    string = GcodArchitecture{x};
    string = strrep(string, '*SETSPEEDLIN*', num2str(speedLin));
    string = strrep(string, '*SETSPEEDCIRC*', num2str(speedCirc));
    string = strrep(string, '*UP*', num2str(0));
    string = strrep(string, '*DOWN*', num2str(-Zdist));
    string = strrep(string, '*FINAL*', num2str(10));
    fprintf(fid,[string, '\n']);
end

fclose(fid);
    
    