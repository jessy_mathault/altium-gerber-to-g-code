clear
clc

fileName = 'wingCut.GTL';

fid = fopen(fileName);

while ~strcmp(fgetl(fid), 'D10*')
end

firstLine = fgetl(fid);
initData = lineReader(firstLine);

sharedData.xoff = initData{1}.num;
sharedData.yoff = initData{2}.num;
sharedData.xi = 0;
sharedData.yi = 0;
sharedData.lxl = initData{1}.num;
sharedData.lyl = initData{2}.num;
sharedData.lxc = 0;
sharedData.lyc = 0;

file.sharedData = sharedData;
file.data = {};

nextLine = fgetl(fid);
while ischar(nextLine)
    lineData = lineReader(nextLine);
    
    for x = 1:size(lineData,2)
        %disp(lineData{x}.char)
        if lineData{x}.char == 'X'
            lineData{x}.num = lineData{x}.num - sharedData.xoff;
            %disp('modifying')
        elseif lineData{x}.char == 'Y'
            lineData{x}.num = lineData{x}.num - sharedData.yoff;
            %disp('modifying')
        end
    end
    file.data{end+1} = lineData;
    nextLine = fgetl(fid);
end


fclose(fid);