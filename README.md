# README #

This converts Altium Gerbers to Gcode while using arcs for tracer application. It is not meant to trace pcbs (for which I recommend Flatcam), but it is a tool to trace line you've drawn in Altium 1:1. Useful for cut-out, silkscreen, woodworking and foam carving. 

====

For this to work properly, set Altium to export as : 

File/Fabrication Outputs/GerberFiles

General/Units : mm

General/Format : 4:3

Layers/ : the ones you want

Advanced/plotterType : sorted

ok

====

file/export/Gerber

check only use arc(G75)

format : RS-274X, settings : 2,3 , metric, absolute, leading

ok

===


So you've got a .gtl which you'll use.


Now open Main.m

write the path and name of the file to open

write the path and name of the gcode file to create

Select the parameters you want included (speed, drop etc)

Click run

Enjoy the gcode, I recommend using this to view the file : https://nraynaud.github.io/webgcode/


### What is this repository for? ###

This is a Altium Gerber to Gcode converter that should evolve to a gui and a small application for CNC lovers who are looking for a simple way to trace lines and arcs. 

### How do I get set up? ###

For this to work properly, set Altium to export as : 

File/Fabrication Outputs/GerberFiles
General/Units : mm
General/Format : 4:3
Layers/ : the ones you want
Advanced/plotterType : sorted
OK

file/export/Gerber
check only use arc(G75)
format : RS-274X, settings : 2,3 , metric, absolute, leading

OK

So you've got a .gtl which you'll use.

Now open Main.m
write the path and name of the file to open
write the path and name of the gcode file to create

Select the parameters you want included (speed, drop etc)

Click run

Enjoy the gcode, I recommend using this to view the file : https://nraynaud.github.io/webgcode/


### Contribution guidelines ###

First repo ever, so I don't know how this works.

### Who do I talk to? ###

Me, Jessy